const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const User = require('../../models/user');

function list(req, res, next){
  let users = [];
  res.json(users);
}

function index(req, res, next){
  let id = req.params.id
  console.log(`El id de usuario es ${id}`);
  let user = {};
  res.json(user);
}

function create(req, res, next){
  let name = req.body.name;
  let lastName = req.body.lastName;
  let email = req.body.email;
  let password = req.body.password;
  console.log(lastName);

  async.parallel({
    salt: (callback)=>{
      bcrypt.genSalt(10, callback);
    },
  },(err,result)=>{
    bcrypt.hash(password,result.salt,(err,hash)=>{
      let user = new User({
        _name:name,
        _lastName:lastName,
        _email:email,
        _password:hash,
        _salt:result.salt
      });
      user.save().then((obj)=>{
        res.json({
          error: false,
          message: 'usuario creado correctamente',
          objs:obj
        });
      }).catch(err => res.status(500).json({
        error: true,
        message: 'usuario no se ha creado correctamente',
        objs: obj
      }));
    });
  });
}

function update(req, res, next){
  res.render('index', { title: 'Se actualizo un elemento' });
}

function destroy(req, res, next){
  res.render('index', { title: 'Se elimino un elemento' });
}

module.exports = {
  index, list, create, update, destroy
}
