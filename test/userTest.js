const supertest = require('supertest');
const app = require('../app');

describe('Endpoint of users',()=>{
  it('should get list users', async ()=>{
    const res = await supertest(app)
    .get('/users/')
    .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiJ9.NWRjNmYzNTQ2ODIxMTkxNTVmNDNjNDU3.q1BEzPqbPijso693kKRCc1WJ5vI9Or7HOH7radobmcE')
    .send();

    expect(res.statustCode).toEqual(200);
  });
})
