const express = require('express');
const router = express.Router();
const actorsController = require('../controllers/actor/actorsController.js')

/* asumir path base /users */
router.get('/:page?',actorsController.list);

router.get('/index/:id',actorsController.index);

router.post('/',actorsController.create);

router.put('/:id',actorsController.update);

router.delete('/:id',actorsController.destroy);

module.exports = router;
